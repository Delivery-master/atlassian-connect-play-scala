package com.atlassian.connect.playscala.controllers

import com.atlassian.connect.playscala.AcConfigured
import play.api.Play
import play.api.mvc.{ RequestHeader, Result }
import play.api.mvc.Results._
import scala.concurrent.Future

trait ActionPredicates extends AcConfigured {

  def OnlyIf[A](p: => Boolean, block: => A): Option[A] = if (p) Some(block) else None

  def NotFoundUnless[A](p: => Boolean, block: => A, unless: => A): A =
    OnlyIf(p, block) getOrElse unless

  def OnlyInDevMode(block: => Result)(implicit request: RequestHeader): Result =
    NotFoundUnless(acConfig.isDev, block, notFound(request))

  def OnlyInDevMode(block: => Future[Result])(implicit request: RequestHeader): Future[Result] =
    NotFoundUnless(acConfig.isDev, block, Future.successful(notFound(request)))

  def notFound(request: RequestHeader): Result = NotFound(_root_.views.html.defaultpages.notFound(request, Play.maybeApplication.flatMap(_.routes)))
}
