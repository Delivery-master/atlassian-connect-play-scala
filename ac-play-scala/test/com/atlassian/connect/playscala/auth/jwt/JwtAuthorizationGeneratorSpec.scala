package com.atlassian.connect.playscala.auth.jwt

import com.atlassian.connect.playscala.AcConfig
import org.specs2.mutable._
import org.specs2.mock.Mockito

import com.atlassian.jwt.{ JwtConstants, SigningAlgorithm }
import com.atlassian.jwt.writer.{ JwtWriter, JwtWriterFactory }
import com.atlassian.connect.playscala.model.{ ClientKey, AcHostModel }
import org.mockito.ArgumentCaptor
import org.specs2.matcher.JsonMatchers
import org.specs2.specification.Scope

class JwtAuthorizationGeneratorSpec extends Specification with Mockito with JsonMatchers {

  val FREDDY = Some("freddy")
  val NOT_SO_SECRET_SECRET = "notSoSecretSecret"
  val MY_PLUGIN_KEY = "myPluginKey"
  val PRODUCT_CONTEXT = "/pc"
  val HOST = "http://somehost:3421"
  val BASE_URL = HOST + PRODUCT_CONTEXT

  val acHostModel = AcHostModel(None, ClientKey("12345"), "9798", BASE_URL, "", None, NOT_SO_SECRET_SECRET)

  trait mocks extends Scope {

    val jwtWriter = mock[JwtWriter]
    val jwtWriterFactory = mock[JwtWriterFactory]
    val acConfig = mock[AcConfig]
    val jwtAuthorizationGenerator = new JwtAuthorizationGenerator(jwtWriterFactory, 180)(acConfig)

    jwtWriterFactory.macSigningWriter(any[SigningAlgorithm], anyString) returns jwtWriter
    acConfig.pluginKey returns MY_PLUGIN_KEY

    def generate(aUrl: String = BASE_URL + "/foo") = jwtAuthorizationGenerator.generate("GET", aUrl, Map.empty[String, Seq[String]], acHostModel, FREDDY)

  }

  "JwtAuthorizationGenerator" should {

    "starts with jwt auth prefix" in new mocks {
      generate() must beSome((_: String) must startWith(JwtConstants.HttpRequests.JWT_AUTH_HEADER_PREFIX))
    }

    "works with relative path" in new mocks {
      generate(PRODUCT_CONTEXT + "/foo") must beSome((_: String) must startWith(JwtConstants.HttpRequests.JWT_AUTH_HEADER_PREFIX))
    }

    "calls writer with correct issuer" in new mocks {
      generate()
      val captor = ArgumentCaptor.forClass(classOf[String])
      there was one(jwtWriter).jsonToJwt(captor.capture())
      captor.getValue must /("iss" -> MY_PLUGIN_KEY)
    }
  }

}
