package com.atlassian.connect.playscala.controllers

import play.api.mvc.{ Action, Controller }
import views.html.ac.internal._

object AcDocumentation extends Controller with DevAction {

  def index() = DevAction {
    Ok(index_doc())
  }

  def descriptor() = DevAction {
    Ok(descriptor_doc())
  }

  def production() = Action {
    Ok(production_doc())
  }

}
